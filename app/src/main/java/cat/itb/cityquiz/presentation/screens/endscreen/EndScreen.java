package cat.itb.cityquiz.presentation.screens.endscreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.screens.gamescreen.GameViewModel;

public class EndScreen extends Fragment {

    @BindView(R.id.welcomText)
    TextView welcomText;
    @BindView(R.id.btnPlayAgain)
    Button btnPlayAgain;
    @BindView(R.id.scoreView)
    TextView scoreView;
    private GameViewModel mViewModel;

    public static EndScreen newInstance() {
        return new EndScreen();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.end_screen_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);
        scoreView.setText(String.valueOf(mViewModel.getScore()));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.btnPlayAgain)
    public void onViewCLicked(View view){
        mViewModel.startGame();
        Navigation.findNavController(view).navigate(R.id.action_endScreen_to_gameScreen);
    }
}
