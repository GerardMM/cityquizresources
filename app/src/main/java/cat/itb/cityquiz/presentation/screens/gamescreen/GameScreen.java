package cat.itb.cityquiz.presentation.screens.gamescreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;

public class GameScreen extends Fragment {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.button)
    MaterialButton button;
    @BindView(R.id.button2)
    MaterialButton button2;
    @BindView(R.id.button3)
    MaterialButton button3;
    @BindView(R.id.button4)
    MaterialButton button4;
    @BindView(R.id.button5)
    MaterialButton button5;
    @BindView(R.id.button6)
    MaterialButton button6;
    private GameViewModel mViewModel;
    MutableLiveData<Game> mutableGame;


    public static GameScreen newInstance() {
        return new GameScreen();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.game_screen_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);
        mutableGame = mViewModel.getGameMutable();
        mutableGame.observe(this, this::display);

    }


    private void display(Game game) {
        if(game.isFinished()){
            Navigation.findNavController(getView()).navigate(R.id.action_gameScreen_to_endScreen);
        }else {
            List<City> cityList = game.getCurrentQuestion().getPossibleCities();
            City city = game.getCurrentQuestion().getCorrectCity();

            String fileName = ImagesDownloader.scapeName(city.getName());
            int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
            imageView.setImageResource(resId);

            getButtonText(cityList);
        }

    }

    private void getButtonText(List<City> cityList) {
        button.setText(cityList.get(0).getName());
        button2.setText(cityList.get(1).getName());
        button3.setText(cityList.get(2).getName());
        button4.setText(cityList.get(3).getName());
        button5.setText(cityList.get(4).getName());
        button6.setText(cityList.get(5).getName());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        //view.findViewById(R.id.btn)
    }

    @OnClick({R.id.button, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6,})
    public void onCLickedView(View view){
        switch (view.getId()){
            case R.id.button:
                questionAnswered(0);
                break;
            case R.id.button2:
                questionAnswered(1);
                break;
            case R.id.button3:
                questionAnswered(2);
                break;
            case R.id.button4:
                questionAnswered(3);
                break;
            case R.id.button5:
                questionAnswered(4);
                break;
            case R.id.button6:
                questionAnswered(5);
                break;
        }
    }

    private void questionAnswered(int answer) {
        mViewModel.questionAnswered(answer);
        display(mViewModel.getGame());
    }
}
