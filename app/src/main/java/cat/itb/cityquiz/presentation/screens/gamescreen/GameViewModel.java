package cat.itb.cityquiz.presentation.screens.gamescreen;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class GameViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    MutableLiveData<Game> gameMutable = new MutableLiveData<>();
    Game game;


    public void startGame() {
        game = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
        gameMutable.postValue(game);
    }
    // TODO: Implement the ViewModel


    public Game getGame() {
        return game;
    }

    public void questionAnswered(int answer) {
        game = gameLogic.answerQuestions(game, answer);
    }

    public MutableLiveData<Game> getGameMutable() {
        return gameMutable;
    }

    public int getScore(){
        return game.getNumCorrectAnswers();
    }
}
